locals {
  terraform_env = terraform.workspace
}

module "telemetry" {
  source           = "./modules/api-to-kinesis-firehose"
  application_name = "telemetry"
}


