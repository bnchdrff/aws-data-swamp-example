output "telemetry_bucket_id" {
  value = module.telemetry.bucket_id
}

output "telemetry_invoke_url" {
  value = module.telemetry.invoke_url
}

output "telemetry_api_key" {
  value = module.telemetry.api_key
  sensitive = true
}

output "telemetry_stream_name" {
  value = module.telemetry.stream_name
}
