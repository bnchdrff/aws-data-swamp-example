data "archive_file" "lambda_zip_addpartitions" {
  type        = "zip"
  output_path = "/tmp/lambda_zip_addpartitions.zip"

  source {
    content = <<EOF
const AWS = require('aws-sdk');

const athena = new AWS.Athena({
  version: '2017-05-18',
  region: '${var.aws_region}',
});

async function runQuery({ QueryString, UniqueRequestId }) {
  console.log('running query', QueryString);
  const params = {
    QueryString,
    ResultConfiguration: {
      OutputLocation: 's3://${aws_s3_bucket.bucket.id}/',
    },
    ClientRequestToken: UniqueRequestId,
  };
  return await athena.startQueryExecution(params).promise();
}

async function getQueryExecution(QueryExecutionId) {
  const params = {
    QueryExecutionId,
  };
  return await athena.getQueryExecution(params).promise();
}

async function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function runAndMonitorQuery(query, id) {
  const { QueryExecutionId } = await runQuery({
    QueryString: query,
    UniqueRequestId: id,
  });
  for (let attempt = 0; attempt < 10; attempt++) {
    const result = await getQueryExecution(QueryExecutionId);
    const state = result.QueryExecution.Status.State;
    switch (state) {
      case 'RUNNING':
      case 'QUEUED': {
        const delayMs = Math.pow(2, attempt + 1) * 100;
        console.log(
          'query is queued or running, retrying in ',
          delayMs,
          'ms',
        );
        await delay(delayMs);
        break;
      }
      case 'SUCCEEDED':
        return true;
      case 'FAILED':
        console.log('query failed');
        throw new Error(result.QueryExecution.Status.StateChangeReason);
      case 'CANCELLED':
        console.log('query is cancelled');
        return;
    }
  }
}

async function createPartition(
  year,
  month,
  day,
  hour,
) {
  const d = new Date().toISOString().split('T');
  const date = d[0]
    .split('-');

  hour = hour || d[1].split(':')[0];
  year = year || date[0];
  month = month || date[1];
  day = day || date[2];
  await runAndMonitorQuery(
    `ALTER TABLE telemetry.telemetry
    ADD PARTITION (year='$${year}',month='$${month}',day='$${day}',hour='$${hour}')
    location 's3://${aws_s3_bucket.bucket.id}/year=$${year}/month=$${month}/day=$${day}/hour=$${hour}'`,
    `automatic-firehose-partitioning-$${year}-$${month}-$${day}-$${hour}`,
  );
}

exports.handler =  async function(event, context) {
  await createPartition();
  return;
};

exports.createPartition = createPartition;
EOF


    filename = "index.js"
  }
}

data "aws_iam_policy_document" "addpartitions" {
  statement {
    sid = "1"

    actions = [
      "lambda:GetPolicy",
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "athena:*",
    ]

    resources = [
      "*",
      "arn:aws:logs:*:*:*",
    ]
  }

  statement {
    sid = "2"

    actions = [
      "glue:CreateDatabase",
      "glue:DeleteDatabase",
      "glue:GetDatabase",
      "glue:GetDatabases",
      "glue:UpdateDatabase",
      "glue:CreateTable",
      "glue:DeleteTable",
      "glue:BatchDeleteTable",
      "glue:UpdateTable",
      "glue:GetTable",
      "glue:GetTables",
      "glue:BatchCreatePartition",
      "glue:CreatePartition",
      "glue:DeletePartition",
      "glue:BatchDeletePartition",
      "glue:UpdatePartition",
      "glue:GetPartition",
      "glue:GetPartitions",
      "glue:BatchGetPartition",
    ]

    resources = [
      "*",
    ]
  }

  statement {
    sid = "3"

    actions = [
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:ListMultipartUploadParts",
      "s3:AbortMultipartUpload",
      "s3:CreateBucket",
      "s3:PutObject",
    ]

    resources = [
      "arn:aws:s3:::aws-athena-query-results-*",
      "arn:aws:s3:::${aws_s3_bucket.bucket.id}*",
    ]
  }
}

module "addpartitions_lambda" {
  source = "github.com/NoRedInk/tf_aws_lambda_scheduled?ref=db0628f00e844d1b80d193288ac8db9fc0229b0c"

  lambda_name         = "addpartitions-${terraform.workspace}"
  runtime             = "nodejs12.x"
  lambda_zipfile      = data.archive_file.lambda_zip_addpartitions.output_path
  source_code_hash    = data.archive_file.lambda_zip_addpartitions.output_base64sha256
  handler             = "index.handler"
  schedule_expression = "cron(0 * * * ? *)"
  iam_policy_document = data.aws_iam_policy_document.addpartitions.json
  timeout             = "30"
}

