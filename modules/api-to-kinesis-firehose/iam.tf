resource "aws_iam_role" "firehose_role" {
  name = "${var.application_name}-firehose-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "firehose.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "firehose" {
  name = "${var.application_name}-firehose-policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement":
  [  
    {    
      "Effect": "Allow",    
      "Action": [    
        "s3:AbortMultipartUpload",    
        "s3:GetBucketLocation",    
        "s3:GetObject",    
        "s3:ListBucket",    
        "s3:ListBucketMultipartUploads",    
        "s3:PutObject"
      ],    
      "Resource": [    
        "${aws_s3_bucket.bucket.arn}",
        "${aws_s3_bucket.bucket.arn}/*"    
      ]  
    },
    {
       "Effect": "Allow",
       "Action": [
         "logs:PutLogEvents"
       ],
       "Resource": [
         "${aws_cloudwatch_log_group.firehose.arn}",
         "${aws_cloudwatch_log_stream.firehose.arn}"
       ]
    }
  ]
}
EOF
}

resource "aws_iam_role" "gateway_execution_role" {
  name = "${var.application_name}-gateway-execution-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "write_policy" {
  name        = "${var.application_name}-write-policy"
  description = "Policy allowing put record/records to a Firehose Stream"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "firehose:PutRecord",
                "firehose:PutRecords"
            ],
            "Resource": [
                "arn:aws:firehose:${var.aws_region}:${data.aws_caller_identity.current.account_id}:deliverystream/${var.application_name}-s3-stream"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "write_role_policy" {
  name = "${var.application_name}-write-policy"

  role = aws_iam_role.gateway_execution_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "firehose:DescribeStream",
                "firehose:PutRecord",
                "firehose:PutRecords"
            ],
            "Resource": [
                "arn:aws:firehose:${var.aws_region}:${data.aws_caller_identity.current.account_id}:deliverystream/${var.application_name}-s3-stream"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "firehose:ListStreams"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
EOF
}

data "aws_iam_policy" "AmazonAPIGatewayPushToCloudWatchLogs" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
}

resource "aws_iam_role_policy_attachment" "gateway_cloudwatch_policy_attach" {
  role       = aws_iam_role.gateway_execution_role.id
  policy_arn = data.aws_iam_policy.AmazonAPIGatewayPushToCloudWatchLogs.arn
}
