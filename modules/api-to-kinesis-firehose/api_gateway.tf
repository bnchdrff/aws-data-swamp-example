resource "aws_api_gateway_rest_api" "mod" {
  name        = "${var.application_name}-api"
  description = "Allows posting of messages to the stream"
}

resource "aws_api_gateway_api_key" "mod" {
  name = "${var.application_name}-api-key"
}

resource "aws_api_gateway_usage_plan" "mod" {
  name = "${var.application_name}-plan"

  api_stages {
    api_id = aws_api_gateway_rest_api.mod.id
    stage  = aws_api_gateway_deployment.mod.stage_name
  }
}

resource "aws_api_gateway_usage_plan_key" "mod" {
  key_id        = aws_api_gateway_api_key.mod.id
  key_type      = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.mod.id
}

resource "aws_api_gateway_deployment" "mod" {
  depends_on  = [aws_api_gateway_integration.put_record]
  rest_api_id = aws_api_gateway_rest_api.mod.id
  stage_name  = var.application_name

  # This guarantees that a new deploy happens any time our config changes:
  # TODO: make this work??
  #stage_description = "${md5(file("main.tf"))}"
  stage_description = "6149a41a4e961f972d1882a6f79d939c"
}
