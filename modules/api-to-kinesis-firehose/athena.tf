resource "aws_s3_bucket" "query_results_bucket" {
  bucket = "${var.application_name}-athena-query-results"
  acl    = "private"
}

resource "aws_athena_database" "db" {
  name   = "telemetry"
  bucket = aws_s3_bucket.query_results_bucket.bucket
}

resource "aws_glue_catalog_table" "aws_glue_catalog_table" {
  name          = "telemetry"
  database_name = "telemetry"

  table_type = "EXTERNAL_TABLE"

  storage_descriptor {
    location      = "s3://${aws_s3_bucket.bucket.id}/"
    input_format  = "org.apache.hadoop.mapred.TextInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.IgnoreKeyTextOutputFormat"

    ser_de_info {
      name                  = "telemetry-stream"
      serialization_library = "org.openx.data.jsonserde.JsonSerDe"

      parameters = {
        "case.insensitive" = false
      }
    }

    columns {
      name = "timestamp"
      type = "timestamp"
    }

    columns {
      name = "env"
      type = "string"
    }

    columns {
      name = "event"
      type = "string"
    }

    columns {
      name = "category"
      type = "string"
    }

    columns {
      name = "source"
      type = "string"
    }

    columns {
      name = "platform"
      type = "string"
    }

    columns {
      name = "version"
      type = "string"
    }

    columns {
      name    = "data"
      type    = "array<struct<level:string,msg:string,component:string,timer:float>>"
    }
  }

  partition_keys {
    name = "year"
    type = "string"
  }
  partition_keys {
    name = "month"
    type = "string"
  }
  partition_keys {
    name = "day"
    type = "string"
  }
  partition_keys {
    name = "hour"
    type = "string"
  }
}
