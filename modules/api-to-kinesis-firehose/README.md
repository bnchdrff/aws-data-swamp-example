Prototyping a general-purpose telemetry service module

## Overview

### Usage

Send arbitrary data like this:

```
 curl -D- -X PUT -H 'Content-Type: application/json' -H "x-api-key: e2RUsAqOwX99SKcAp02WG7MgwqiZMJU91NnWYO7X"  --data '{"timestamp":"2019-04-06 12:45:12.666","env":"dev","event":"click","category":"soft","source":"altavista","platform":"multics","version":"2005","data":{"whatever":"goes","here":3.3}}' 'https://hibvvppyc1.execute-api.us-east-1.amazonaws.com/telemetry/record?stream-name=telemetry-dev-s3-stream'
```

... and then it's stored in an s3 bucket, ready to be analyzed by Athena and Quicksight.
#### Athena setup

An Athena database, s3 bucket for query results, and a table are all created by the definition in `athena.tf`

##### Partitioning

Partitions are added hourly by a scheduled Lambda.

#### Quicksight setup

Permissions are weird by default; you need to grant Quicksight permission to access all S3 buckets:

Upper-right user icon -> click on "Manage QuickSight"
Left-hand-side menu -> click on "Account settings"
Under "Connected products & services" -> click on "Add or remove"
To the right of "Amazon S3" -> click on "Choose S3 buckets"
click on "Select all" and then click "Select buckets" button
click "Update"

Now you can go back to the start page `https://us-east-1.quicksight.aws.amazon.com/sn/start`
click "Manage data" button on the right
click "New data set"
click "Athena"


### Architecture

```
Client
HTTP request with arbitrary structured json log payload

->

API Gateway

->

Kinesis Firehose delivery stream

->

S3
```
