resource "aws_s3_bucket" "bucket" {
  bucket = "${var.application_name}-event-stream"
  acl    = "private"
}

resource "aws_kinesis_firehose_delivery_stream" "s3_stream" {
  name        = "${var.application_name}-s3-stream"
  destination = "extended_s3"

  extended_s3_configuration {
    role_arn            = aws_iam_role.firehose_role.arn
    bucket_arn          = aws_s3_bucket.bucket.arn
    prefix              = "year=!{timestamp:yyyy}/month=!{timestamp:MM}/day=!{timestamp:dd}/hour=!{timestamp:HH}/"
    error_output_prefix = "errors/result=!{firehose:error-output-type}/year=!{timestamp:yyyy}/month=!{timestamp:MM}/day=!{timestamp:dd}/hour=!{timestamp:HH}/"
    buffer_size         = 1
    buffer_interval     = 60

    cloudwatch_logging_options {
      enabled         = true
      log_group_name  = aws_cloudwatch_log_group.firehose.name
      log_stream_name = aws_cloudwatch_log_stream.firehose.name
    }
  }
}

resource "aws_cloudwatch_log_group" "firehose" {
  name = "${var.application_name}-firehose"
}

resource "aws_cloudwatch_log_stream" "firehose" {
  name           = "${var.application_name}-firehose-log-stream"
  log_group_name = aws_cloudwatch_log_group.firehose.name
}

resource "aws_iam_role_policy_attachment" "firehose-attach" {
  role       = aws_iam_role.firehose_role.name
  policy_arn = aws_iam_policy.firehose.arn
}
