resource "aws_api_gateway_resource" "record" {
  rest_api_id = aws_api_gateway_rest_api.mod.id
  parent_id   = aws_api_gateway_rest_api.mod.root_resource_id
  path_part   = "record"
}

resource "aws_api_gateway_method" "put_record" {
  rest_api_id          = aws_api_gateway_rest_api.mod.id
  resource_id          = aws_api_gateway_resource.record.id
  http_method          = "PUT"
  authorization        = "NONE"
  api_key_required     = true
  request_validator_id = aws_api_gateway_request_validator.telemetry.id

  request_models = {
    "application/json" = aws_api_gateway_model.telemetry.name
  }
}

resource "aws_api_gateway_integration" "put_record" {
  rest_api_id             = aws_api_gateway_rest_api.mod.id
  resource_id             = aws_api_gateway_resource.record.id
  http_method             = aws_api_gateway_method.put_record.http_method
  type                    = "AWS"
  integration_http_method = "POST"
  uri                     = "arn:aws:apigateway:${var.aws_region}:firehose:action/PutRecord"
  passthrough_behavior    = "WHEN_NO_TEMPLATES"
  credentials             = aws_iam_role.gateway_execution_role.arn

  request_parameters = {
    "integration.request.header.Content-Type" = "'application/x-amz-json-1.1'"
  }

  # Passthrough the JSON request
  # TODO: add a computed timestamp field
  # see: https://forums.aws.amazon.com/thread.jspa?threadID=238417
  request_templates = {
    "application/json" = <<EOF
#set($payload="$input.json('$')
")
{
  "DeliveryStreamName": "$input.params('stream-name')",
  "Record": {
    "Data": "$util.base64Encode($payload)"
  }
}
EOF
  }
}

resource "aws_api_gateway_method_response" "put_record_ok" {
  depends_on  = [aws_api_gateway_method.put_record]
  rest_api_id = aws_api_gateway_rest_api.mod.id
  resource_id = aws_api_gateway_resource.record.id
  http_method = aws_api_gateway_method.put_record.http_method
  status_code = "200"
}

resource "aws_api_gateway_integration_response" "put_record" {
  rest_api_id = aws_api_gateway_rest_api.mod.id
  resource_id = aws_api_gateway_resource.record.id
  http_method = aws_api_gateway_method.put_record.http_method
  status_code = aws_api_gateway_method_response.put_record_ok.status_code

  # Passthrough the JSON response
  response_templates = {
    "application/json" = <<EOF
EOF
  }
}

resource "aws_api_gateway_model" "telemetry" {
  rest_api_id  = aws_api_gateway_rest_api.mod.id
  name         = "telemetry"
  description  = "telemetry payload"
  content_type = "application/json"

  schema = <<EOF
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "title": "Streetcred telemetry",
  "required": [
    "timestamp",
    "env",
    "event",
    "category",
    "source",
    "platform",
    "version"
  ],
  "properties": {
    "timestamp": {
      "type": "string",
      "title": "Client-provided timestamp",
      "default": "",
      "pattern": "^(.*)$"
    },
    "env": {
      "type": "string",
      "title": "Environment",
      "default": "",
      "pattern": "^(.*)$"
    },
    "event": {
      "type": "string",
      "title": "Event type",
      "default": "",
      "pattern": "^(.*)$"
    },
    "category": {
      "type": "string",
      "title": "Event category",
      "default": "",
      "pattern": "^(.*)$"
    },
    "source": {
      "type": "string",
      "title": "Source name",
      "default": "",
      "pattern": "^(.*)$"
    },
    "platform": {
      "type": "string",
      "title": "Client platform",
      "default": "",
      "pattern": "^(.*)$"
    },
    "version": {
      "type": "string",
      "title": "Client version",
      "default": "",
      "pattern": "^(.*)$"
    },
    "data": {
      "type": "object",
      "title": "Additional data",
      "description": "Arbitrary additional data to be stored as a blob",
      "default": null
    }
  }
}
EOF
}

resource "aws_api_gateway_request_validator" "telemetry" {
  name                        = "telemetry"
  rest_api_id                 = aws_api_gateway_rest_api.mod.id
  validate_request_body       = true
  validate_request_parameters = true
}
