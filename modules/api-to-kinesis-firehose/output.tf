output "invoke_url" {
  value = "${aws_api_gateway_deployment.mod.invoke_url}"
}

output "api_key" {
  value = "${aws_api_gateway_api_key.mod.value}"
}

output "stream_name" {
  value = "${aws_kinesis_firehose_delivery_stream.s3_stream.name}"
}

output "bucket_id" {
  value = "${aws_s3_bucket.bucket.id}"
}
