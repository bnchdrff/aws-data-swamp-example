variable aws_region {
  type    = string
  default = "us-east-1"
}

variable "application_name" {
  default = "app"
}
